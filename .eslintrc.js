module.exports = {
    extends: 'standard',
    rules: {
        'no-unused-vars': 'warn',
        'handle-callback-err': 'off',
        'no-multi-spaces': 'off',
        'curly': 'off',
        'semi': 'off'
    },
    'env': {
        'browser': true,
        'node': true,
        'es6': true,
        'mocha': true
    }
}