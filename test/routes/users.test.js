const app = require('../..')
const request = require('supertest')
const mocha = require('mocha')
const { describe, it } = mocha

describe('users routes', () => {

  describe('GET /users', () => {
    it('should responds 403, no api key found', done => {
      request(app)
        .get('/users')
        .expect(403, done)
    })
  })

  describe('GET /users', () => {
    it('should responds 403, invalid api key', done => {
      request(app)
        .get('/users')
        .set('Authorization', 'Bearer invalidToken')
        .expect(403, done)
    })
  })

  describe('POST /users', () => {
    it('should responds 403, no api key found', done => {
      request(app)
        .post('/users')
        .send({ email: 'test@gmail.com', firstname: 'Jean', lastname: 'Bon', password: 'azerty123456' })
        .expect(403, done)
    })
  })

  describe('POST /users', () => {
    it('should responds 403, invalid api key', done => {
      request(app)
        .post('/users')
        .send({ email: 'test@gmail.com', firstname: 'Jean', lastname: 'Bon', password: 'azerty123456' })
        .set('Authorization', 'Bearer invalidToken')
        .expect(403, done)
    })
  })

  describe('PUT /users/:id', () => {
    it('should responds 403, no api key found', done => {
      request(app)
        .put('/users/1')
        .send({ email: 'test@gmail.com', password: 'azerty123456' })
        .expect(403, done)
    })
  })

  describe('PUT /users/:id', () => {
    it('should responds 403, invalid api key', done => {
      request(app)
        .put('/users/1')
        .send({ email: 'test@gmail.com', password: 'azerty123456' })
        .set('Authorization', 'Bearer invalidToken')
        .expect(403, done)
    })
  })


  describe('DELETE /users/:id', () => {
    it('should responds 403, no api key found', done => {
      request(app)
        .delete('/users/1')
        .send({ email: 'test@gmail.com', password: 'azerty123456' })
        .expect(403, done)
    })
  })

  describe('DELETE /users/:id', () => {
    it('should responds 403, invalid api key', done => {
      request(app)
        .delete('/users/1')
        .send({ email: 'test@gmail.com', password: 'azerty123456' })
        .set('Authorization', 'Bearer invalidToken')
        .expect(403, done)
    })
  })

})
