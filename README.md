### Install:
    
    npm install
    copy .env.sample .env

### Start Production

    npm run start
    
### Start Development

    npm run dev
