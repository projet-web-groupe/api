const Sequelize = require('sequelize')

module.exports = sequelize => {
  class ApiKey extends Sequelize.Model {
  }

  ApiKey.init({
    key: { type: Sequelize.STRING(255) }
  }, { tableName: 'api_key', underscored: true, timestamps: false, sequelize })

  ApiKey.associate = models => {
    ApiKey.belongsTo(models.Role, { as: 'role' })
  }

  return ApiKey
}
