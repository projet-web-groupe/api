const fs = require('fs')
const path = require('path')
const Sequelize = require('sequelize')
const { dialect, host, port, database, user, password, logging } = require('../../config/db')
const db = {}

const sequelize = new Sequelize(
  database,
  user,
  password,
  {
    dialect,
    host,
    port,
    logging
  }
)

fs.readdirSync(__dirname).filter((file) =>
  file !== 'index.js'
).forEach((file) => {
  const model = sequelize.import(path.join(__dirname, file))
  db[model.name] = model
})

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db)
  }
})

db.sequelize = sequelize
db.Sequelize = Sequelize

module.exports = db
