const Sequelize = require('sequelize')
const crypto = require('crypto')

module.exports = sequelize => {
  class User extends Sequelize.Model {
  }

  User.init({
    email: { type: Sequelize.STRING(255) },
    password: { type: Sequelize.STRING(255) },
    firstname: { type: Sequelize.STRING(255) },
    lastname: { type: Sequelize.STRING(255) },
    location: { type: Sequelize.STRING(255) },
    status: { type: Sequelize.ENUM('active', 'pending', 'banned'), defaultValue: 'pending' },
    token: { type: Sequelize.STRING(255) }
  }, { tableName: 'user', underscored: true, sequelize })

  User.prototype.comparePassword = function (password) {
    const originalHash = this.password.split('$')[1]
    const salt = this.password.split('$')[0]
    const hash = crypto.pbkdf2Sync(password, salt, 10000, 64, 'sha512').toString('hex')
    return hash === originalHash
  }

  return User
}
