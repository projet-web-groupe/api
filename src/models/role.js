const Sequelize = require('sequelize')

module.exports = sequelize => {
  class Role extends Sequelize.Model {
  }

  Role.init({
    name: { type: Sequelize.STRING(255), unique: true }
  }, { tableName: 'role', underscored: true, timestamps: false, sequelize })

  return Role
}
