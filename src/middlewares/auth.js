const { ApiKey } = require('../models')

module.exports = {
  permit(...allowed) {
    const isAllowed = role => allowed.indexOf(role) > -1

    // return a middleware
    return (req, res, next) => {
      if (req.role && isAllowed(req.role))
        next() // role is allowed, so continue on the next middleware
      else {
        res.status(403).send({ error: 'Not allowed' }) // forbidden
      }
    }
  },

  async authenticate(req, res, next) {
    // Verify token send
    let key = req.headers.authorization

    if (!key || key.trim() === '' || key.split(' ')[1] === undefined)
      return res.status(403).send({ error: 'No API key found' }).end()

    key = key.split(' ')[1]

    const apiKey = await ApiKey.findOne({ where: { key } })
    if (!apiKey)
      return res.status(403).send({ error: 'API key incorrect' }).end()

    const role = await apiKey.getRole()
    req.role = role.name

    next()
  }
}
