const Joi = require('@hapi/joi')

const schemas = {
  userCreate: Joi.object({
    email: Joi.string().required(),
    password: Joi.string().required(),
    firstname: Joi.string().required(),
    lastname: Joi.string().required(),
    location: Joi.string().required()
  }),
  userUpdate: Joi.object({
    email: Joi.string().optional(),
    password: Joi.string().optional(),
    firstname: Joi.string().optional(),
    lastname: Joi.string().optional(),
    location: Joi.string().optional()
  }),
  userLogin: Joi.object({
    email: Joi.string().required(),
    password: Joi.string().required()
  }),
  userParam: Joi.object({
    id: Joi.number().required()
  }),
  userConfirmEmail: Joi.object({
    email: Joi.string().required(),
    token: Joi.string().required()
  })
}

module.exports = schemas
