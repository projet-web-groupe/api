module.exports = {
  from: process.env.MAIL_FROM || '',
  apiKey: process.env.MAIL_API_KEY || '',
  domain: process.env.MAIL_DOMAIN || '',
  host: process.env.MAIL_HOST || ''
}
