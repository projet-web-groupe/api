const env = require('dotenv')
env.config({ debug: process.env.DEBUG })
const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const { sequelize, Role } = require('./models')
const defaultRoles = require('../init/defaultRoles')
const { sendMailHTML } = require('../src/utils/sendmail')
const { confirmEmail } = require('../src/emails')

app.use(bodyParser.json())
app.use('/', require('./routes'))

sequelize.sync().then(() => {
  Role.bulkCreate(defaultRoles, { ignoreDuplicates: true })
  app.listen(process.env.PORT, () => console.log('API started on port ' + process.env.PORT))
})

module.exports = app
