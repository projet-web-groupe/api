const crypto = require('crypto')
const base64url = require('base64url');

module.exports = {
  hashPassword(password) {
    const salt = crypto.randomBytes(16).toString('hex')
    const hash = crypto.pbkdf2Sync(password, salt, 10000, 64, 'sha512').toString('hex')
    return [salt, hash].join('$')
  },

  randomToken() {
    return base64url(crypto.randomBytes(20))
  }
}
