const { from, apiKey, domain, host } = require('../config/email')
const nodemailer = require('nodemailer')
const { MailgunTransport } = require('mailgun-nodemailer-transport')

let transporter = nodemailer.createTransport(new MailgunTransport({
  auth: {
    domain,
    apiKey
  },
  hostname: host
}))

module.exports = {
  async sendMailHTML(to, { subject, html }) {
    return await transporter.sendMail({ from, to, subject, html })
  },

  async sendMailText(to, { subject, text }) {
    return await transporter.sendMail({ from, to, subject, text })
  }
}
