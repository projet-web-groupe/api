const router = require('express').Router()
const { authenticate, permit } = require('../middlewares/auth')

router.use('/users', authenticate, permit('website', 'admin'))
router.use('/auth/login', authenticate, permit('website', 'admin'))
router.use('/auth/register', authenticate, permit('website', 'admin'))

router.use('/users', require('./users'))
router.use('/auth', require('./auth'))

module.exports = router
