const router = require('express').Router()
const middleware = require('../middlewares')
const { userCreate, userUpdate, userParam } = require('../middlewares/schema')
const { User } = require('../models')

/**
 * @api {get} /users Request Users information
 * @apiName GetAllUsers
 * @apiGroup User
 *
 * @apiParam {String} privateKey Private API Key
 *
 * @apiSuccess {Array} users Users list
 */
router.get('/', async (req, res) => {
  const users = await User.findAll()
  return res.send({ users })
})

/**
 * @api {get} /users/:id Request User information
 * @apiName GetUser
 * @apiGroup User
 *
 * @apiParam {Number} id Users unique ID.
 *
 * @apiParam {String} privateKey Private API Key
 *
 * @apiSuccess {Integer} id Id of the User.
 * @apiSuccess {String} firstname Firstname of the User.
 * @apiSuccess {String} lastname Lastname of the User.
 * @apiSuccess {String} email Lastname of the User.
 * @apiSuccess {String} role Role of the User.
 */
router.get('/:id', middleware(userParam, 'params'), async (req, res) => {
  const { id } = req.params
  const user = await User.findByPk(id)
  if (!user) {
    return res.status(404).send({ error: 'User not found!' })
  }
  return res.send({ user })
})

/**
 * @api {post} /users Create an User
 * @apiName AddUser
 * @apiGroup User
 *
 * @apiSuccess {String} firstname Firstname of the User.
 * @apiSuccess {String} lastname Lastname of the User.
 * @apiSuccess {String} email Lastname of the User.
 * @apiSuccess {String} role Role of the User.
 */
router.post('/', middleware(userCreate, 'body'), async (req, res) => {
  const user = await User.create(req.body)
  if (user)
    return res.send({ success: true, message: 'User created!' })
  return res.status(500).send({ success: false, error: 'Failed to create user.' })
})

/**
 * @api {update} /users/:id Update an User
 * @apiName UpdateUser
 * @apiGroup User
 *
 * @apiParam {Number} id Users unique ID.
 */
router.put('/:id', middleware(userParam, 'params'), middleware(userUpdate, 'body'), async (req, res) => {
  const user = await User.findByPk(req.params.id)
  if (!user)
    return res.status(404).send({ success: false, error: 'User not found.' })

  const updated = await user.update(req.body)
  if (updated)
    return res.send({ success: true, message: 'User updated!' })

  return res.status(500).send({ success: false, error: 'Failed to update user.' })
})

/**
 * @api {delete} /users/:id Delete an User
 * @apiName DeleteUser
 * @apiGroup User
 *
 * @apiParam {Number} id Users unique ID.
 */
router.delete('/:id', middleware(userParam, 'params'), async (req, res) => {
  const { id } = req.params
  const user = await User.destroy({ where: { id } })

  if (!user)
    return res.status(404).send({ error: 'User not found!' })
  return res.send({ success: true, message: 'User deleted.' })
})

module.exports = router
