const router = require('express').Router()
const middleware = require('../middlewares')
const { userLogin, userCreate, userConfirmEmail } = require('../middlewares/schema')
const { User } = require('../models')
const { hashPassword, randomToken } = require('../utils/user')
const { sendMailHTML } = require('../utils/sendmail')
const { confirmEmail } = require('../emails')

/**
 * @api {post} /auth/login Login an user
 * @apiName LoginUser
 * @apiGroup Auth
 *
 * @apiParam {String} email Email.
 * @apiParam {String} password Password.
 *
 * @apiSuccess {String} token JWT Token
 * @apiSuccess {Boolean} success true
 *
 * @apiError {Boolean} success false
 */
router.post('/login', middleware(userLogin, 'body'), async (req, res) => {
  const { email, password } = req.body
  const user = await User.findOne({ where: { email } })
  if (!user)
    return res.status(404).send({ success: false, error: 'User not found!' })

  if (user.status === 'pending')
    return res.status(400).send({ success: false, error: 'Veuillez confirmer votre e-mail.' })

  if (user.status === 'banned')
    return res.status(403).send({ success: false, error: 'Compte banni.' })

  if (!user.comparePassword(password)) return res.status(400).send({ success: false, error: 'Password incorrect!' })

  delete user.password

  return res.send({ success: true, user })
})

/**
 * @api {post} /auth/register Register an user
 * @apiName RegisterUser
 * @apiGroup Auth
 *
 * @apiParam {String} email Email.
 * @apiParam {String} lastname Lastname.
 * @apiParam {String} firstname Firstname.
 * @apiParam {String} password Password.
 *
 * @apiSuccess {String} token JWT Token
 * @apiSuccess {Boolean} success true
 *
 * @apiError {Boolean} success false
 */
router.post('/register', middleware(userCreate, 'body'), async (req, res) => {
  const { email, password, lastname, firstname, location } = req.body
  const check = await User.findOne({ where: { email } })
  if (check)
    return res.status(400).send({ success: false, error: 'Email already use!' })

  const user = await User.create({
    email: email.toLowerCase(),
    password: hashPassword(password),
    lastname,
    firstname,
    location,
    token: randomToken()
  })
  if (user) {
    confirmEmail.html = confirmEmail.html.replace('%url%', `http://localhost:3000/auth/confirm-email?email=${ user.email }&token=${ user.token }`)
    await sendMailHTML(user.email, confirmEmail)
    delete user.password
    return res.send({ success: true, user })
  }

  return res.status(500).send({ success: false, error: 'Failed to register user' })
})

/**
 * @api {get} /auth/confirm-email Confirm email address
 * @apiName ConfirmEmail
 * @apiGroup Auth
 *
 * @apiParam {String} email Email.
 * @apiParam {String} token Token.
 *
 * @apiSuccess {String} token JWT Token
 * @apiSuccess {Boolean} success true
 *
 * @apiError {Boolean} success false
 */
router.get('/confirm-email', middleware(userConfirmEmail, 'query'), async (req, res) => {
  const { email, token } = req.query
  console.log(email)
  const user = await User.findOne({ where: { email } })
  if (!user)
    return res.status(404).send({ success: false, error: 'User not found!' })

  if (user.status === 'active')
    return res.status(400).send({ success: false, error: 'Already active.' })

  if (user.token !== token)
    return res.status(400).send({ success: false, error: 'Invalid token.' })

  if (user.status !== 'pending') {
    return res.status(400).send({ success: false, error: 'Invalid link.' })
  }

  user.token = ''
  user.status = 'active'
  await user.save()

  return res.send({ success: true, user })
})

module.exports = router
